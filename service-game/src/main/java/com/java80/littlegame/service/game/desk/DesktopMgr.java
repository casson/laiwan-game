package com.java80.littlegame.service.game.desk;

import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 本节点所有房间桌子信息， 暂存java内存，可以放缓存
 * 
 *
 */
public class DesktopMgr {
	private static ConcurrentHashMap<Integer, Desktop> desks = new ConcurrentHashMap<>();
	private static ConcurrentHashMap<Long, Integer> userRooms = new ConcurrentHashMap<>();

	public static Desktop getDesktop(long userId) {
		Integer rid = userRooms.get(userId);
		if (rid != null) {
			return desks.get(rid);
		}
		return null;
	}

	public static void addDesktop(Integer roomId, Desktop desk, List<Long> userIds) {
		desks.put(roomId, desk);
		for (long uid : userIds) {
			userRooms.put(uid, roomId);
		}
	}

	public static Desktop removeDesktop(Integer roomId) {
		Desktop desktop = desks.remove(roomId);
		List<Long> playerIds = desktop.setting.getPlayerIds();
		for (long id : playerIds) {
			userRooms.remove(id);
		}
		return desktop;
	}

	public static Desktop getDesktop(Integer roomId) {
		return desks.get(roomId);
	}
}
