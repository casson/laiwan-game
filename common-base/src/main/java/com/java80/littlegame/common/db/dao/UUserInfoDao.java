package com.java80.littlegame.common.db.dao;

import com.java80.littlegame.common.db.dao.base.BaseUserDaoInterface;
import com.java80.littlegame.common.db.entity.UUserInfo;

public interface UUserInfoDao extends BaseUserDaoInterface<UUserInfo> {
	public UUserInfo login(String userName, String password) throws Exception;

	public UUserInfo findByLoginName(String loginName) throws Exception;
}
