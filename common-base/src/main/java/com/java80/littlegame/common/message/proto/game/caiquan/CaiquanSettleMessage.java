package com.java80.littlegame.common.message.proto.game.caiquan;

import com.java80.littlegame.common.message.proto.BaseMsg;
import com.java80.littlegame.common.message.proto.ProtoList;

public class CaiquanSettleMessage extends BaseMsg {

	@Override
	public int getType() {
		// TODO Auto-generated method stub
		return ProtoList.MSG_TYPE_GAME;
	}

	@Override
	public int getCode() {
		// TODO Auto-generated method stub
		return ProtoList.MSG_CODE_CAIQUANASETTLE;
	}

	private long winner;// 没有就平局

	public long getWinner() {
		return winner;
	}

	public void setWinner(long winner) {
		this.winner = winner;
	}

}
